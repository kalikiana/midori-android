package org.midori_browser.midori

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView

class CustomAdapter(var items: ArrayList<HistoryWeb>, var listener: ClickListener): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val vista = LayoutInflater.from(p0.context).inflate(R.layout.list_history, p0 ,false)
        val viewHolder = ViewHolder(vista, listener)

        return viewHolder
    }

    override fun getItemCount():Int{
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val item  = items[p1]
        holder.url?.text = item.urlWeb
    }

    class ViewHolder(vista: View, listener: ClickListener):RecyclerView.ViewHolder(vista), OnClickListener{

        var url:TextView? = null
        var listener:ClickListener? = null

        init {
            this.url = vista.findViewById(R.id.nameUrl)
            this.listener = listener
            vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener?.onClick(v!!, adapterPosition)
        }
    }
}