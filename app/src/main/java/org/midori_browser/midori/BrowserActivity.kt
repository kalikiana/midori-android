package org.midori_browser.midori

import android.app.DownloadManager
import android.content.*
import android.net.Uri
import android.os.*
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.*
import kotlinx.android.synthetic.main.activity_browser.urlBar
import kotlinx.android.synthetic.main.activity_browser.webView
import org.midori_browser.midori.database.HistoryCRUD
import org.midori_browser.midori.database.HistoryDatabase


class BrowserActivity : AppCompatActivity()  {

    var downloadListener: DownloadListener? = null
    var webViews: WebView? = null
    private val CONTEXT_MENU_ID_DOWNLOAD_IMAGE = 1
    var historyDatabase: HistoryCRUD? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setSupportActionBar(findViewById(R.id.toolbar))
        registerForContextMenu(webView)

        var web = intent.getStringExtra("TAG_HISTORY")

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        requestDesktopSite(false)
        webSettings.databaseEnabled = true
        webSettings.setAppCacheEnabled(true)
        webView.setPadding(0,0,0,0)
        webSettings.domStorageEnabled = true
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
        webView.webViewClient = WebViewClient(this)
        webView.webChromeClient = WebChromeClient(this)
        webView.isLongClickable = true


        val openTabs = (getSharedPreferences("config", Context.MODE_PRIVATE).getString(
           "openTabs", null
            ) ?: getString(R.string.appWebsite)).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        webView.loadUrl(openTabs.first())

        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item, completion
        )
        urlBar.setAdapter(adapter)
        urlBar.onItemClickListener = AdapterView.OnItemClickListener { parent,
                                                                       _,
                                                                       position,
                                                                       _ ->
            val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            im.hideSoftInputFromWindow(parent.applicationWindowToken, 0)
            loadUrlOrSearch(parent.getItemAtPosition(position).toString())
        }
        urlBar.setOnEditorActionListener { v, actionId, event ->
            if ((event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) || actionId == EditorInfo.IME_ACTION_DONE) {
                val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                im.hideSoftInputFromWindow(v.applicationWindowToken, 0)
                loadUrlOrSearch(urlBar.text.toString())
                true
            } else {
                false
            }
        }
        urlBar.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {}
        })

        if (web != null) {
            urlBar.setText(web.toString())
            webView.loadUrl(web.toString())
        }


        urlBar.setupClearButtonWithAction()
        createDownloaderListener()
        onDownloadComplete()
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val result = webView.hitTestResult
        val url:String = webView.originalUrl.toString()
        webView.hitTestResult?.let{
            when(it.type){
                WebView.HitTestResult.IMAGE_TYPE,
                WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE -> {
                    menu?.setHeaderTitle(url)
                    menu?.add(0,  CONTEXT_MENU_ID_DOWNLOAD_IMAGE, 0, R.string.download_image)?.setOnMenuItemClickListener {
                        val imgUrl = result.extra
                        if (URLUtil.isNetworkUrl(imgUrl) && URLUtil.isValidUrl(imgUrl)){
                            val request:DownloadManager.Request = DownloadManager.Request(Uri.parse(imgUrl))
                            request.allowScanningByMediaScanner()
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            val downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                            downloadManager.enqueue(request)
                        }
                        false
                    }
                }
                else -> Toast.makeText(this, "App does not yet handle target type: ${it.type}", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        urlBar.onItemClickListener
        downloadListener
    }

    override fun onPause() {
        super.onPause()
        webViews = findViewById(R.id.webView)
        webViews!!.setDownloadListener(downloadListener)
    }

    override fun onResume() {
        super.onResume()
        historyDatabase = HistoryCRUD(this)
    }

    private fun onDownloadComplete(){

        val onComplete = object: BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                Toast.makeText(context, "File Downloaded", Toast.LENGTH_SHORT).show()
            }
        }
        registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }

    private fun createDownloaderListener(){

        val permissionCheck = PermissionCheck()

        if (permissionCheck.readAndWriteExternalStorage(this)) {
            downloadListener = DownloadListener { url,
                                                  _,
                                                  contentDescription,
                                                  mimetype,
                                                  _ ->
                val request = DownloadManager.Request(Uri.parse(url))
                val fileName = URLUtil.guessFileName(url, contentDescription, mimetype)
                request.allowScanningByMediaScanner()
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                request.setVisibleInDownloadsUi(true)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                val dManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                dManager.enqueue(request)
                Toast.makeText(this, "Downloading File " , Toast.LENGTH_SHORT).show()
            }
        }else{
            Log.e("Error: ", "Error Downloading")
        }
    }

    private fun loadUrlOrSearch(text: String) {
        webView.loadUrl(magicUri(text) ?: uriForSearch(text))
    }

    fun magicUri(text: String): String? {
        if (" " in text) {
            return null
        } else if (text.startsWith("http")) {
            return text
        } else if (text == "localhost" || "." in text) {
            return "http://" + text
        }
        return null
    }

    val locationEntrySearch = "https://duckduckgo.com/?q=%s"
    fun uriForSearch(keywords: String? = null, search: String? = null): String {
        val uri = search ?: locationEntrySearch
        val escaped = Uri.encode(keywords ?: "", ":/")
        // Allow DuckDuckGo to distinguish Midori and in turn share revenue
        if (uri == "https://duckduckgo.com/?q=%s") {
            return "https://duckduckgo.com/?q=$escaped&t=midori"
        } else if ("%s" in uri) {
            return uri.format(escaped)
        }
        return uri + escaped
    }

    val completion = listOf("www.midori-browser.org", "example.com", "duckduckgo.com")

    fun requestDesktopSite(desktopSite: Boolean) {
        webView.settings.apply {
            userAgentString = null // Reset to default
            userAgentString = userAgentString + " " + getString(R.string.userAgentVersion)
            if (desktopSite) {
                // Websites look for "Android" and "Mobile" keywords to decide what's not desktop
                val mobileOS = userAgentString.substring(userAgentString.indexOf("("), userAgentString.indexOf(")") + 1)
                userAgentString = userAgentString.replace(mobileOS, "(X11; Linux x86_64)").replace(" Mobile", "")
            }
            useWideViewPort = desktopSite
            loadWithOverviewMode = desktopSite
        }
    }

    fun showDownload(){
        val intent = Intent()
        intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.actionShare -> {
            val intent = Intent(Intent.ACTION_SEND)
            val uri = Uri.parse(webView.url)
            intent.putExtra(Intent.EXTRA_TEXT, uri.toString())
            intent.type = "text/plain"
            startActivity(Intent.createChooser(intent, getString(R.string.actionShare)))
            true
        }
        R.id.actionClearPrivateData -> {
            @Suppress("DEPRECATION")
            CookieManager.getInstance().removeAllCookie()
            WebStorage.getInstance().deleteAllData()
            webView.clearCache(true)
            true
        }
        R.id.actionRequestDesktopSite -> {
            item.isChecked = !item.isChecked
            requestDesktopSite(item.isChecked)
            webView.reload()
            true
        }
        R.id.actionTabs -> {
            val intent = Intent(this, ActivityTabs::class.java)
            startActivity(intent)
            true
        }
        R.id.download -> {
            showDownload()
            true
        }
        R.id.goBack -> {
            if (webView.canGoBack()){
                webView.goBack()
                Toast.makeText(this, webView.url, Toast.LENGTH_SHORT).show()
            }
            true
        }
        R.id.goForward ->{
            if (webView.canGoForward()){
                webView.goForward()
                Toast.makeText(this, webView.url, Toast.LENGTH_SHORT).show()
            }
            true
        }
        R.id.history -> {
            listHistory()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    fun AutoCompleteTextView.setupClearButtonWithAction() {

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                val clearIcon = if (editable?.isNotEmpty() == true) R.drawable.ic_close_black_24dp else 0
                setCompoundDrawablesWithIntrinsicBounds(0, 0, clearIcon, 0)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                    this.setText("")
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })
    }

    override fun onBackPressed() {
        if(webView.canGoBack()){
            webView.goBack()
        }else {
            super.onBackPressed()
        }
    }

    private fun listHistory(){
        historyDatabase = HistoryCRUD(this)
        var webList: ArrayList<HistoryWeb> = ArrayList()
        val webBack = webView.copyBackForwardList()
        val count= webBack.size
        var i =0

        while(i < count){
            val item:WebHistoryItem = webBack.getItemAtIndex(i)
            historyDatabase?.addVisitHistoryEntry(item.url.toString(), item.title, HistoryWeb(item.url.toString(), item.title.toString()))
            i++
        }
        val intent = Intent(this, History::class.java)
        startActivity(intent)
    }
}



