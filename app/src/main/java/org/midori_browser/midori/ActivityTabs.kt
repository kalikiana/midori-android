package org.midori_browser.midori

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_tabs.*

class ActivityTabs : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabs)
        setSupportActionBar(findViewById(R.id.toolbar))
        registerForContextMenu(webView)

        var actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        var web = intent.getStringExtra("TAG")
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.loadUrl(web)
        urlBar.setText(web)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_2, menu)
        return super.onCreateOptionsMenu(menu)
    }

}