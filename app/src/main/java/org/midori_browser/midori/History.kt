package org.midori_browser.midori

import android.content.DialogInterface
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AlertDialogLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import org.midori_browser.midori.database.HistoryCRUD
import org.midori_browser.midori.database.HistoryDatabase

class History : AppCompatActivity() {

    var lista: RecyclerView? = null
    var adaptador: CustomAdapter? = null
    var layoutManager:RecyclerView.LayoutManager? = null
    var listado: ArrayList<HistoryWeb> = ArrayList()
    private var database: HistoryCRUD? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        setSupportActionBar(findViewById(R.id.toolbar))

        var actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)

        database = HistoryCRUD(this)
        listado = database!!.listHistoryEntries()

        lista = findViewById(R.id.list)
        lista?.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager

        adaptador = CustomAdapter(listado, object: ClickListener{
            override fun onClick(view: View, index: Int) {
                val web = listado.get(index)
                val intent = Intent(applicationContext, BrowserActivity::class.java)
                intent.putExtra("TAG_HISTORY", web.urlWeb.toString())
                startActivity(intent)
            }
        })
        lista?.adapter = adaptador
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_history, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem)= when(item.itemId) {
        R.id.actionDelete -> {

            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("Do you want to delete the History?")
                .setCancelable(false)
                .setPositiveButton("Yes", (DialogInterface.OnClickListener { dialog, which ->
                    database = HistoryCRUD(this)
                    database!!.deleteHistory()
                    val intent = Intent(this, BrowserActivity::class.java)
                    startActivity(intent)
                }))
                .setNegativeButton("Cancel", (DialogInterface.OnClickListener {
                        dialog,
                        id ->
                    dialog.cancel()
                }))

            val alert = builder.create()
            alert.setTitle("History")
            alert.show()
            val btnPositive = alert.getButton(DialogInterface.BUTTON_POSITIVE)
            btnPositive.setTextColor(Color.BLACK)
            val btnNegative = alert.getButton(DialogInterface.BUTTON_NEGATIVE)
            btnNegative.setTextColor(Color.BLACK)
            true
        }else -> {
            super.onOptionsItemSelected(item)
        }
    }
}















