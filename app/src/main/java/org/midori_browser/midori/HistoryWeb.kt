package org.midori_browser.midori

import android.os.Parcel
import android.os.Parcelable

class HistoryWeb(url:String?, title:String?) {

    var urlWeb: String? = null
    var titleWeb: String? = null
    var timeDate: Int = 0
    var id: Int = 0

    init{
        this.urlWeb = url
        this.titleWeb = title
    }

    constructor(id: Int, url: String?, title: String?, timeDate: Int):this(url, title){
        this.id = id
        this.urlWeb = url
        this.titleWeb = title
        this.timeDate = timeDate


    }
}


