package org.midori_browser.midori.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.icu.util.TimeZone
import org.midori_browser.midori.HistoryWeb
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_TIME_VISITED
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_TITLE
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_URL
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.TABLE_HISTORY


class HistoryCRUD(context: Context): HistoryRepository {

    private var helper: HistoryDatabase? = null

    init {
        helper = HistoryDatabase(context)
    }

    override fun deleteHistory() {
        val db: SQLiteDatabase = helper!!.writableDatabase
        db.delete(HistoryContract.Companion.Entry.TABLE_HISTORY, null, null)
        db.close()
    }

    override fun deleteHistoryEntry(item: HistoryWeb){
        val db: SQLiteDatabase = helper!!.writableDatabase

        db.delete(HistoryContract.Companion.Entry.TABLE_HISTORY, "id = ?", arrayOf(item.id.toString()))
        db.close()
    }

    override fun addVisitHistoryEntry(url: String, title: String, item: HistoryWeb){
        val db:SQLiteDatabase = helper!!.readableDatabase
        val values = ContentValues().apply {
            put(KEY_TITLE, title)
            put(KEY_TIME_VISITED, System.currentTimeMillis())
        }

        db.query(
            false,
            TABLE_HISTORY,
            arrayOf(KEY_URL),
            "$KEY_URL = ?",
            arrayOf(url),
            null,
            null,
            null,
            "1"
            ).use {
                if (it.count > 0){
                    db.update(TABLE_HISTORY, values, "$KEY_URL = ?", arrayOf(url))
                }else{
                    addHistory(HistoryWeb(item.urlWeb, item.titleWeb))
                }
        }
    }

    override fun addHistory(item: HistoryWeb){
        val db:SQLiteDatabase = helper!!.writableDatabase
        val values = ContentValues()

        values.put(HistoryContract.Companion.Entry.KEY_URL, item.urlWeb)
        values.put(HistoryContract.Companion.Entry.KEY_TITLE, item.titleWeb)
        values.put(HistoryContract.Companion.Entry.KEY_TIME_VISITED, System.currentTimeMillis())

        val newRowId = db.insert(HistoryContract.Companion.Entry.TABLE_HISTORY,null, values )
        db.close()
    }

    override fun listHistoryEntries(): ArrayList<HistoryWeb>{
        val webs:ArrayList<HistoryWeb> = ArrayList()
        val db: SQLiteDatabase = helper!!.readableDatabase

        val column = arrayOf(
            HistoryContract.Companion.Entry.KEY_ID,
            HistoryContract.Companion.Entry.KEY_URL,
            HistoryContract.Companion.Entry.KEY_TITLE,
            HistoryContract.Companion.Entry.KEY_TIME_VISITED
        )

        val cursor: Cursor = db.query(
            HistoryContract.Companion.Entry.TABLE_HISTORY,
            column,
            null,
            null,
            null,
            null,
            "$KEY_TIME_VISITED DESC",
            "150"
        )

        while (cursor.moveToNext()){
            webs.add(
                HistoryWeb(
                    cursor.getInt(cursor.getColumnIndexOrThrow(HistoryContract.Companion.Entry.KEY_ID)),
                    cursor.getString(cursor.getColumnIndexOrThrow(HistoryContract.Companion.Entry.KEY_URL)),
                    cursor.getString(cursor.getColumnIndexOrThrow(HistoryContract.Companion.Entry.KEY_TITLE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(HistoryContract.Companion.Entry.KEY_TIME_VISITED)))
            )
        }
        db.close()

        return webs
    }
}