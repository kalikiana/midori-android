package org.midori_browser.midori.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import org.midori_browser.midori.database.HistoryContract.Companion.DATABASE_VERSION
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.DATABASE_NAME
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_ID
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_TIME_VISITED
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_TITLE
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.KEY_URL
import org.midori_browser.midori.database.HistoryContract.Companion.Entry.Companion.TABLE_HISTORY

class HistoryDatabase(
    context: Context
): SQLiteOpenHelper(context,
    DATABASE_NAME,
    null,
    DATABASE_VERSION){

    override fun onCreate(db: SQLiteDatabase?) {

        val createHistoryTable = "CREATE TABLE $TABLE_HISTORY (" +
                "$KEY_ID INTEGER PRIMARY KEY," +
                "$KEY_URL TEXT," +
                "$KEY_TITLE TEXT," +
                "$KEY_TIME_VISITED INTEGER" +
                ")"
            db?.execSQL(createHistoryTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_HISTORY")
        onCreate(db)
    }
}