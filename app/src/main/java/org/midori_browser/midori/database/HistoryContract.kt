package org.midori_browser.midori.database

import android.provider.BaseColumns

class HistoryContract {

    companion object{

        val DATABASE_VERSION = 2
        class Entry: BaseColumns{
            companion object{

                val DATABASE_NAME = "historyManager"
                val TABLE_HISTORY = "history"
                val KEY_ID = "id"
                val KEY_URL = "url"
                val KEY_TITLE = "title"
                val KEY_TIME_VISITED = "time"
            }

        }

    }
}