package org.midori_browser.midori.database

import org.midori_browser.midori.HistoryWeb

interface HistoryRepository {

    fun deleteHistory()

    fun deleteHistoryEntry(item: HistoryWeb)

    fun addVisitHistoryEntry(url: String, title: String, item: HistoryWeb)

    fun listHistoryEntries(): ArrayList<HistoryWeb>

    fun addHistory(item: HistoryWeb)

}