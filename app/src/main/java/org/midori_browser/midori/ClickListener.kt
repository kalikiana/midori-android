package org.midori_browser.midori

import android.view.View

interface ClickListener {

    fun onClick(view: View, index: Int)
}